package com.jlo.credittracker.utils;

public final class StringUtil {
	private StringUtil() {
		// TODO Auto-generated constructor stub
	}
	
	/**
	 * 
	 * @param count
	 * @return
	 */
	public static String addLeadingZero(int count, Long value) {
		 return String.format("%0" + count + "d", value);
		
	}

}
