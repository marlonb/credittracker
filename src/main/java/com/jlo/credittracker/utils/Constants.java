package com.jlo.credittracker.utils;

public final class Constants {
	private Constants() {
		// TODO Auto-generated constructor stub
	}

	public static final String OFFSET_DATE_TIME_COLUMN_DEFINITION = "TIMESTAMP WITH TIME ZONE";
	
	//System-defined privileges
	public static final String ADMIN_PRIVILEGE = "ADMIN_PRIVILEGE";//SuperUser
	public static final String READ_PRIVILEGE = "READ_PRIVILEGE";
	public static final String WRITE_PRIVILEGE = "WRITE_PRIVILEGE";
	public static final String APPROVAL_PRIVILEGE = "APPROVAL_PRIVILEGE";


}
