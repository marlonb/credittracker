package com.jlo.credittracker.helpers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Service;

@Service
/**
 * Use this to get the ApplicationContext object
 * @author marlonb
 *
 */
public class ContextWrapper {

    private static ApplicationContext context;

    @Autowired
    public ContextWrapper(ApplicationContext ac) {
    	ContextWrapper.context = ac;
    }

	public static ApplicationContext getContext() {
        return ContextWrapper.context;
    }

}