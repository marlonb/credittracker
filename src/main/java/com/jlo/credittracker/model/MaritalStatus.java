package com.jlo.credittracker.model;

public enum MaritalStatus {
	SINGLE,
	MARRIED,
	LEGALLY_SEPARATED,
	DIVORCED,
	WIDOWED
}
