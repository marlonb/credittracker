package com.jlo.credittracker.jpa.listeners;

import javax.persistence.PrePersist;

import org.springframework.dao.DataAccessException;

import com.jlo.credittracker.helpers.ContextWrapper;
import com.jlo.credittracker.jpa.domain.Client;
import com.jlo.credittracker.jpa.repositories.ClientRepository;
import com.jlo.credittracker.utils.StringUtil;


public class ClientListener {
	private static final String CLIENT_REPOSITORY = "ClientRepository";

	@PrePersist
	public void clientPrePersist(Client client) {
		setClientNumber(client);
	}
	
	/**
	 * Generate the client number and set it to the client.
	 *
	 * @param client
	 */
	private void setClientNumber(Client client) {
		try {
			final Long value = this.geClientRepository().getNextClientNumber();
			final String clientNumber = StringUtil.addLeadingZero(20, value);
			client.setClientNumber(clientNumber);
		} catch (DataAccessException ex) {
			// throw error
		}
		
	}
	
	private ClientRepository geClientRepository() {
        return ContextWrapper.getContext().getBean(CLIENT_REPOSITORY, ClientRepository.class);
    }

}
