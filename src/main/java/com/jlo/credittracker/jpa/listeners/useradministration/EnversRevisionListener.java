package com.jlo.credittracker.jpa.listeners.useradministration;

import org.hibernate.envers.RevisionListener;

import com.jlo.credittracker.jpa.domain.useradministration.AuditEnversInfo;

public class EnversRevisionListener implements RevisionListener {

	@Override
	public void newRevision(Object revisionEntity) {
		/**
		 * NOTE: Refactor to use Spring security
		 */
		AuditEnversInfo auditEnversInfo = (AuditEnversInfo) revisionEntity;
        auditEnversInfo.setUserId("0");
		
	}

}
