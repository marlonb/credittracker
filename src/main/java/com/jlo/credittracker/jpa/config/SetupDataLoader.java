package com.jlo.credittracker.jpa.config;

import static com.jlo.credittracker.utils.Constants.ADMIN_PRIVILEGE;
import static com.jlo.credittracker.utils.Constants.APPROVAL_PRIVILEGE;
import static com.jlo.credittracker.utils.Constants.READ_PRIVILEGE;
import static com.jlo.credittracker.utils.Constants.WRITE_PRIVILEGE;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.OffsetDateTime;
import java.time.ZoneOffset;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
//import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.jlo.credittracker.enums.ClientStatus;
import com.jlo.credittracker.jpa.domain.Client;
import com.jlo.credittracker.jpa.domain.ClientContact;
import com.jlo.credittracker.jpa.domain.ClientIndividual;
import com.jlo.credittracker.jpa.domain.ContactAddress;
import com.jlo.credittracker.jpa.domain.useradministration.AppUser;
import com.jlo.credittracker.jpa.domain.useradministration.Privilege;
import com.jlo.credittracker.jpa.domain.useradministration.Role;
import com.jlo.credittracker.jpa.repositories.ClientRepository;
import com.jlo.credittracker.jpa.repositories.useradministration.AppUserRepository;
import com.jlo.credittracker.jpa.repositories.useradministration.PrivilegeRepository;
import com.jlo.credittracker.jpa.repositories.useradministration.RoleRepository;
import com.jlo.credittracker.model.ClientType;
import com.jlo.credittracker.model.MaritalStatus;
import com.jlo.credittracker.model.Sex;

@Component
public class SetupDataLoader implements ApplicationListener<ContextRefreshedEvent> {

    private boolean alreadySetup = false;
    
    @Autowired
    private ClientRepository clientRepository;
    
    @Autowired
    private RoleRepository roleRepository;
    
    @Autowired
    private AppUserRepository appUserRepository;
    
    @Autowired
    private PrivilegeRepository privilegeRepository;
    
    //@Autowired
    //private PasswordEncoder passwordEncoder;


    // API

    @Override
    @Transactional
    public void onApplicationEvent(final ContextRefreshedEvent event) {
        if (alreadySetup) {
            return;
        }

        
        
        // == create initial privileges
        final Privilege adminPrivilege = createPrivilegeIfNotFound(ADMIN_PRIVILEGE);
        final Privilege readPrivilege = createPrivilegeIfNotFound(READ_PRIVILEGE);
        final Privilege writePrivilege = createPrivilegeIfNotFound(WRITE_PRIVILEGE);
        final Privilege approvalPrivilege = createPrivilegeIfNotFound(APPROVAL_PRIVILEGE);
        
        
        // == create initial roles
        final List<Privilege> adminPrivileges = new ArrayList<Privilege>(Arrays.asList(adminPrivilege, readPrivilege, writePrivilege));
        final List<Privilege> approvalPrivileges = new ArrayList<Privilege>(Arrays.asList(approvalPrivilege, readPrivilege, writePrivilege));
        final List<Privilege> staffUserPrivileges = new ArrayList<Privilege>(Arrays.asList(readPrivilege, writePrivilege));
        final List<Privilege> userPrivileges = new ArrayList<Privilege>(Arrays.asList(readPrivilege));
        
        final Role adminRole = createRoleIfNotFound("ROLE_ADMIN", adminPrivileges);
        final Role managerRole = createRoleIfNotFound("ROLE_MANAGER", approvalPrivileges);
        final Role encoderRole = createRoleIfNotFound("ROLE_ENCODER", staffUserPrivileges);
        final Role userRole = createRoleIfNotFound("ROLE_USER", userPrivileges);

        // == create initial user	
        final AppUser user1 = createUserIfNotFound("AppUser1", "AppUser1FN", "AppUser1LN", "appUser1@y.com", "password", new ArrayList<Role>(Arrays.asList(adminRole)));
        createUserIfNotFound("AppUser2", "AppUser2FN", "AppUser2LN", "appUser2@y.com", "password", new ArrayList<Role>(Arrays.asList(managerRole)));
        createUserIfNotFound("AppUser3", "AppUser3FN", "AppUser3LN", "appUser3@y.com", "password", new ArrayList<Role>(Arrays.asList(encoderRole)));
        createUserIfNotFound("AppUser4", "AppUser4FN", "AppUser4LN", "appUser4@y.com", "password", new ArrayList<Role>(Arrays.asList(userRole)));
        
        
        // Create client
        createClientIfNotFound(user1);
        
        //Find client
        Client client = clientRepository.findById(1L).orElse(null);
		if (client != null) {
			System.out.println("Client not null");
		}
        
        alreadySetup = true;
    }

    @Transactional
    private final Client createClientIfNotFound(AppUser user1) {
		Client client = clientRepository.findById(1L).orElse(null);
		if (client == null) {
			ClientIndividual clientIndividual = new ClientIndividual();
			clientIndividual.setDateOfBirth(LocalDate.of(1980, 11, 01));
			clientIndividual.setEmail("marlonsbautista@gmail.com");
			clientIndividual.setFathersName("Alex Bautista");
			clientIndividual.setFirstName("Marlon");
			clientIndividual.setLastName("Bautista");
			clientIndividual.setMaritalStatus(MaritalStatus.MARRIED);
			clientIndividual.setMiddleName("Sia");
			clientIndividual.setMobileNo("09989977192");
			clientIndividual.setMothersMaidenName("ATS");
			clientIndividual.setSex(Sex.M);
			
			ContactAddress contactAddress = new ContactAddress();
			contactAddress.setBlockName("Block 1");
			contactAddress.setBlockNo("1");
			contactAddress.setAddr1("Address1");
			contactAddress.setAddr2("Address2");
			contactAddress.setAddr3("Address3");
			contactAddress.setAddr4("Address4");
			contactAddress.setAddr5("Address5");
			contactAddress.setCity("Manila");
			contactAddress.setCountry("PH");
			contactAddress.setDistrict("District1");
			contactAddress.setStreetName("Sesame Street");
			contactAddress.setStreetNo("Stree1");
			contactAddress.setProvince("Province1");
			contactAddress.setPostalCode("123");
			
			ClientContact clientContact = new ClientContact();
			clientContact.setContactDepartment("DEPT1");
			clientContact.setContactPerson("ContactPerson1");
			clientContact.setContactAddress(contactAddress);
			
			
			
			client = new Client();
			client.setStatus(ClientStatus.PENDING.getValue());
			client.setClientType(ClientType.INDIVIDUAL);
			client.setSubmittedBy(user1);
			client.setSubmittedOnDate(OffsetDateTime.of(LocalDateTime.now(), ZoneOffset.UTC));
			client.setClientIndividual(clientIndividual);
			client.setClientContact(clientContact);
			client = clientRepository.save(client);
		}
		
		return client;
				
	}

	@Transactional
    private final Privilege createPrivilegeIfNotFound(final String name) {
        Privilege privilege = privilegeRepository.findByName(name);
        if (privilege == null) {
            privilege = new Privilege(name);
            privilege = privilegeRepository.save(privilege);
        }
        return privilege;
    }

    @Transactional
    private final Role createRoleIfNotFound(final String name, final Collection<Privilege> privileges) {
        Role role = roleRepository.findByName(name);
        if (role == null) {
            role = new Role(name, name);
        }
        role.setPrivileges(privileges);
        role = roleRepository.save(role);
        return role;
    }

    @Transactional
    private final AppUser createUserIfNotFound(final String username,    		
    		final String firstName, 
    		final String lastName, 
    		final String email,
    		final String password, 
    		final Collection<Role> roles) {
    	AppUser appUser = appUserRepository.findByEmail(email);
        if (appUser == null) {
        	appUser = new AppUser();                       
			appUser.setUsername(username);
			appUser.setFirstname(firstName);
			appUser.setLastname(lastName);
			appUser.setEmail(email);
			appUser.setPassword(password);
			appUser.setActiveYn(true);
			appUser.setDeletedYn(false);
			appUser.setPasswordNeverExpiresYn(true);
        }
        appUser.setRoles(roles);
        appUser = appUserRepository.save(appUser);
        return appUser;
    }

}