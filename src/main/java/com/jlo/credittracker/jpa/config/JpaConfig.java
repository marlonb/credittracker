package com.jlo.credittracker.jpa.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.domain.AuditorAware;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;

import com.jlo.credittracker.jpa.listeners.ClientListener;

@Configuration
@EnableJpaAuditing(auditorAwareRef = "auditorAware", 
dateTimeProviderRef = "dateTimeProvider")
public class JpaConfig {
//    @Bean
//    public AuditingEntityListener createAuditingListener() {
//        return new AuditingEntityListener();
//    }
//
//    @Bean
//    public ClientListener clientListener() {
//        return new ClientListener();
//    }
//    
    
    @Bean
    public AuditorAware<String> auditorAware() {
        return new AuditorAwareImpl();
    }
}