package com.jlo.credittracker.jpa.repositories.useradministration;

import org.springframework.data.repository.CrudRepository;

import com.jlo.credittracker.jpa.domain.useradministration.Permission;

public interface PermissionRepository extends CrudRepository<Permission, Long> {

}
