package com.jlo.credittracker.jpa.repositories.useradministration;

import org.springframework.data.repository.CrudRepository;

import com.jlo.credittracker.jpa.domain.useradministration.AppUser;

public interface AppUserRepository extends CrudRepository<AppUser, Long> {
	AppUser findByEmail(String email);

    @Override
    void delete(AppUser appUser);

}
