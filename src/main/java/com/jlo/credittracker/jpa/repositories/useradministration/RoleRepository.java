package com.jlo.credittracker.jpa.repositories.useradministration;

import org.springframework.data.repository.CrudRepository;

import com.jlo.credittracker.jpa.domain.useradministration.Role;

public interface RoleRepository extends CrudRepository<Role, Long> {
	Role findByName(String name);

    @Override
    void delete(Role role);

}
