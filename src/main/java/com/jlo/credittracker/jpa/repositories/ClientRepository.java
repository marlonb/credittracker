package com.jlo.credittracker.jpa.repositories;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import com.jlo.credittracker.jpa.domain.Client;



@Repository(value = "ClientRepository")
public interface ClientRepository extends PagingAndSortingRepository<Client, Long> {
	Long getNextClientNumber();
	

}
