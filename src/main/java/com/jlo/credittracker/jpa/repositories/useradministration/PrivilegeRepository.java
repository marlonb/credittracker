package com.jlo.credittracker.jpa.repositories.useradministration;

import org.springframework.data.jpa.repository.JpaRepository;

import com.jlo.credittracker.jpa.domain.useradministration.Privilege;

public interface PrivilegeRepository extends JpaRepository<Privilege, Long> {

    Privilege findByName(String name);

    @Override
    void delete(Privilege privilege);

}

