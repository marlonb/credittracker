package com.jlo.credittracker.jpa.domain;

import java.io.Serializable;
import java.time.LocalDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.OneToOne;
import javax.validation.constraints.Size;

import org.hibernate.envers.Audited;

import com.jlo.credittracker.model.MaritalStatus;
import com.jlo.credittracker.model.Sex;


@Audited
@Entity()
public class ClientIndividual extends AbstractPersistableEntity<Long> implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6676797367173310910L;

	@Column(name = "FIRST_NAME", length = 100)
    @Size(max = 100)
    private String firstName;

    @Column(name = "MIDDLE_NAME", length = 100)
    @Size(max = 100)
    private String middleName;

    @Column(name = "LAST_NAME", length = 100)
    @Size(max = 100)
    private String lastName;

    @Column(name = "MAIDEN_NAME", length = 100)
    @Size(max = 100)
    private String maidenName;

    @Column(name = "MOTHERS_MAIDEN_NAME", length = 100)
    @Size(max = 100)
    private String mothersMaidenName;

    @Column(name = "FATHERS_NAME", length = 100)
    @Size(max = 100)
    private String fathersName;

    @Column(name = "MARITAL_STATUS", length = 20)
    @Enumerated(EnumType.STRING)
    private MaritalStatus maritalStatus;

    @Column(name = "SEX", length = 1)
    @Enumerated(EnumType.STRING)
    private Sex sex;
    
    @Column(name = "MOBILE_NO", length = 50, nullable = false, unique = true)
    @Size(max = 50)
    private String mobileNo;
	
	@Column(name = "EMAIL", length = 50, nullable = true, unique = true)
	@Size(max = 50)
    private String email;
    
    @Column(name = "DATE_OF_BIRTH", nullable = true)
    private LocalDate dateOfBirth;
    
    @OneToOne(mappedBy = "clientIndividual")
    private Client client;
    
    public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getMiddleName() {
		return middleName;
	}
	public void setMiddleName(String middleName) {
		this.middleName = middleName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public String getMaidenName() {
		return maidenName;
	}
	public void setMaidenName(String maidenName) {
		this.maidenName = maidenName;
	}
	public String getMothersMaidenName() {
		return mothersMaidenName;
	}
	public void setMothersMaidenName(String mothersMaidenName) {
		this.mothersMaidenName = mothersMaidenName;
	}
	public String getFathersName() {
		return fathersName;
	}
	public void setFathersName(String fathersName) {
		this.fathersName = fathersName;
	}
	public MaritalStatus getMaritalStatus() {
		return maritalStatus;
	}
	public void setMaritalStatus(MaritalStatus maritalStatus) {
		this.maritalStatus = maritalStatus;
	}
	public Sex getSex() {
		return sex;
	}
	public void setSex(Sex sex) {
		this.sex = sex;
	}

	public String getMobileNo() {
		return mobileNo;
	}
	public void setMobileNo(String mobileNo) {
		this.mobileNo = mobileNo;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public LocalDate getDateOfBirth() {
		return dateOfBirth;
	}
	public void setDateOfBirth(LocalDate dateOfBirth) {
		this.dateOfBirth = dateOfBirth;
	}


	public Client getClient() {
		return client;
	}
	public void setClient(Client client) {
		this.client = client;
	}
    
    

}
