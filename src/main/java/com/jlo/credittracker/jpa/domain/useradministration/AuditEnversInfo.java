package com.jlo.credittracker.jpa.domain.useradministration;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import org.hibernate.envers.DefaultRevisionEntity;
import org.hibernate.envers.RevisionEntity;

import com.jlo.credittracker.jpa.listeners.useradministration.EnversRevisionListener;

@Entity
@RevisionEntity(EnversRevisionListener.class)
@Table(name = "ENV_AUDIT_ENVERS_INFO")
public class AuditEnversInfo extends DefaultRevisionEntity {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5224346699060226909L;
	
	@Column(name = "USER_ID")
    private String userId;
	
	@Column(name = "REVISION_TYPE_VALUE")
	private String revisionTypeValue;

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

	public String getRevisionTypeValue() {
		return revisionTypeValue;
	}

	public void setRevisionTypeValue(String revisionTypeValue) {
		this.revisionTypeValue = revisionTypeValue;
	}
    
    

}
