package com.jlo.credittracker.jpa.domain.useradministration;

import java.io.Serializable;
import java.util.Collection;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;

import com.jlo.credittracker.jpa.domain.AbstractPersistableEntity;

@Entity
public class Role extends AbstractPersistableEntity<Long> implements Serializable {    
	/**
	 * 
	 */
	private static final long serialVersionUID = 5207811475114586984L;
	
    @Column(name = "NAME", unique = true, nullable = false, length = 100)
    private String name;

    @Column(name = "DESCRIPTION", nullable = false, length = 500)
    private String description;

    @ManyToMany(mappedBy = "roles")
    private Collection<AppUser> appUsers;

//    @ManyToMany(fetch = FetchType.EAGER)
//    @JoinTable(name = "ROLE_PERMISSION", joinColumns = @JoinColumn(name = "ROLE_ID"), inverseJoinColumns = @JoinColumn(name = "PERMISSION_ID"))
//    private Set<Permission> permissions = new HashSet<>();

    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(name = "ROLES_PRIVILEGES", 
    joinColumns = @JoinColumn(name = "ROLE_ID", referencedColumnName = "ID"), 
    inverseJoinColumns = @JoinColumn(name = "PRIVILEGE_ID", referencedColumnName = "ID"))
    private Collection<Privilege> privileges;
    
    
    
	public Role() {
		super();
		// TODO Auto-generated constructor stub
	}
	
    public Role(final String name, final String description) {
        super();
        this.name = name;
        this.description = description;
    }

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Collection<AppUser> getAppUsers() {
		return appUsers;
	}

	public void setAppUsers(Collection<AppUser> appUsers) {
		this.appUsers = appUsers;
	}

	public Collection<Privilege> getPrivileges() {
		return privileges;
	}

	public void setPrivileges(Collection<Privilege> privileges) {
		this.privileges = privileges;
	}

	
	
//	public Boolean getDisabledYn() {
//		return disabledYn;
//	}
//
//	public void setDisabledYn(Boolean disabledYn) {
//		this.disabledYn = disabledYn;
//	}

//	public Set<Permission> getPermissions() {
//		return permissions;
//	}

//	public void setPermissions(Set<Permission> permissions) {
//		this.permissions = permissions;
//	}
	
//    public boolean addPermission(final Permission permission) {
//        return this.permissions.add(permission);
//    }

	
	

}
