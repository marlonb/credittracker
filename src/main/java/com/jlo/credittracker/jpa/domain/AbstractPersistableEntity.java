package com.jlo.credittracker.jpa.domain;


import java.io.Serializable;
import java.time.OffsetDateTime;

import javax.persistence.Column;
import javax.persistence.EntityListeners;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;
import javax.persistence.Version;

import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedBy;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.domain.Persistable;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import static com.jlo.credittracker.utils.Constants.OFFSET_DATE_TIME_COLUMN_DEFINITION;;


/**
 * Entity Base class
 * 
 * @author marlonb
 *
 * @param <PK>
 */
@MappedSuperclass
@EntityListeners(AuditingEntityListener.class)
public abstract class AbstractPersistableEntity<PK extends Serializable> implements Persistable<Long> {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	@Version
	private long version;
	
	@CreatedBy
    private String createdBy;

    @CreatedDate
    @Column(columnDefinition= OFFSET_DATE_TIME_COLUMN_DEFINITION)
    private OffsetDateTime  creationDate;

    @LastModifiedBy
    private String lastModifiedBy;

    @LastModifiedDate
    @Column(columnDefinition= OFFSET_DATE_TIME_COLUMN_DEFINITION)
    private OffsetDateTime  lastModifiedDate;
    
    
    
	public long getVersion() {
		return version;
	}

	public void setVersion(long version) {
		this.version = version;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public OffsetDateTime getCreationDate() {
		return creationDate;
	}

	public void setCreationDate(OffsetDateTime creationDate) {
		this.creationDate = creationDate;
	}

	public String getLastModifiedBy() {
		return lastModifiedBy;
	}

	public void setLastModifiedBy(String lastModifiedBy) {
		this.lastModifiedBy = lastModifiedBy;
	}

	public OffsetDateTime getLastModifiedDate() {
		return lastModifiedDate;
	}

	public void setLastModifiedDate(OffsetDateTime lastModifiedDate) {
		this.lastModifiedDate = lastModifiedDate;
	}

	@Override
	public Long getId() {
		return this.id;
	}

	protected void setId(final Long id) {
		System.out.println("marlon id: " + id);
		this.id = id;
		
	}

	@Override
	public boolean isNew() {
		return this.id == null;
	}

}
