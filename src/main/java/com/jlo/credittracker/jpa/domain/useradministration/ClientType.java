package com.jlo.credittracker.jpa.domain.useradministration;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;

import com.jlo.credittracker.jpa.domain.AbstractPersistableEntity;

@Entity
public class ClientType extends AbstractPersistableEntity<Long> implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -8152308241093543764L;
	

	@Column(name = "CLIENT_TYPE", nullable = false)
	private ClientType clientType;
	
}
