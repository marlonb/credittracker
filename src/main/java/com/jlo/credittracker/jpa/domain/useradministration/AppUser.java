package com.jlo.credittracker.jpa.domain.useradministration;

import static com.jlo.credittracker.utils.Constants.OFFSET_DATE_TIME_COLUMN_DEFINITION;

import java.io.Serializable;
import java.time.OffsetDateTime;
import java.util.Collection;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;

import org.hibernate.envers.Audited;
import org.hibernate.envers.NotAudited;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jlo.credittracker.jpa.domain.AbstractPersistableEntity;

@Entity
@Audited
public class AppUser extends AbstractPersistableEntity<Long> implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = -6310569352193014172L;


	private final static Logger logger = LoggerFactory.getLogger(AppUser.class);


	@Column(name = "EMAIL", nullable = false, length = 50, unique = true)
	private String email;

	@Column(name = "USERNAME", nullable = false, length = 100, unique = true)
	private String username;

	@Column(name = "FIRSTNAME", nullable = false, length = 100)
	private String firstname;

	@Column(name = "LASTNAME", nullable = false, length = 100)
	private String lastname;

	@Column(name = "PASSWORD", length = 60, nullable = false)
	@NotAudited
	private String password;
	
	@Column(name = "ACTIVE_YN", nullable = false)
	private boolean activeYn;
	
	@Column(name = "DELETED_YN", nullable = false)
	private boolean deletedYn;


//	@ManyToMany(fetch = FetchType.EAGER)
//	@JoinTable(name = "APPUSER_ROLE", joinColumns = @JoinColumn(name = "APPUSER_ID"), inverseJoinColumns = @JoinColumn(name = "ROLE_ID"))
//	private Set<Role> roles = new HashSet<>();

	@Column(name = "LAST_TIME_PASSWORD_UPDATED", columnDefinition= OFFSET_DATE_TIME_COLUMN_DEFINITION)
	private OffsetDateTime lastTimePasswordUpdated;

	@Column(name = "PASSWORD_NEVER_EXPIRES_YN", nullable = false)
	private boolean passwordNeverExpiresYn;
	
    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(name = "users_roles", 
    joinColumns = @JoinColumn(name = "APPUSER_ROLES", referencedColumnName = "ID"), inverseJoinColumns = @JoinColumn(name = "ROLE_ID", referencedColumnName = "ID"))
    @NotAudited
    private Collection<Role> roles;
    
	
	

	public AppUser() {
		super();
		this.passwordNeverExpiresYn = true;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getFirstname() {
		return firstname;
	}

	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}

	public String getLastname() {
		return lastname;
	}

	public void setLastname(String lastname) {
		this.lastname = lastname;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	
	public boolean isActiveYn() {
		return activeYn;
	}

	public void setActiveYn(boolean activeYn) {
		this.activeYn = activeYn;
	}

	public boolean isDeletedYn() {
		return deletedYn;
	}

	public void setDeletedYn(boolean deletedYn) {
		this.deletedYn = deletedYn;
	}

	

	public Collection<Role> getRoles() {
		return roles;
	}

	public void setRoles(Collection<Role> roles) {
		this.roles = roles;
	}

	public OffsetDateTime getLastTimePasswordUpdated() {
		return lastTimePasswordUpdated;
	}

	public void setLastTimePasswordUpdated(OffsetDateTime lastTimePasswordUpdated) {
		this.lastTimePasswordUpdated = lastTimePasswordUpdated;
	}

	public boolean isPasswordNeverExpiresYn() {
		return passwordNeverExpiresYn;
	}

	public void setPasswordNeverExpiresYn(boolean passwordNeverExpiresYn) {
		this.passwordNeverExpiresYn = passwordNeverExpiresYn;
	}
	
	
	
	

}
