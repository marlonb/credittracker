package com.jlo.credittracker.jpa.domain;

import static com.jlo.credittracker.utils.Constants.OFFSET_DATE_TIME_COLUMN_DEFINITION;

import java.io.Serializable;
import java.time.OffsetDateTime;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedNativeQueries;
import javax.persistence.NamedNativeQuery;
import javax.persistence.OneToOne;

import org.hibernate.envers.Audited;
import org.hibernate.envers.NotAudited;

import com.jlo.credittracker.enums.ClientStatus;
import com.jlo.credittracker.jpa.domain.useradministration.AppUser;
import com.jlo.credittracker.jpa.listeners.ClientListener;
import com.jlo.credittracker.model.ClientType;

@Entity
@Audited
@EntityListeners(ClientListener.class)
@NamedNativeQueries({
	@NamedNativeQuery(
	name = "Client.getNextClientNumber",
	query = "SELECT nextval('CLIENT_NUMBER_SEQ')")
})
public class Client extends AbstractPersistableEntity<Long> implements Serializable {	
    /**
	 * 
	 */
	private static final long serialVersionUID = -2139154675082934026L;


	@Column(name = "CLIENT_NO", length = 20, unique = true, nullable = false, updatable = false)
	@NotAudited
    private String clientNumber;   
    

    /**
     * A value from {@link ClientStatus}
     */
    @Column(name = "STATUS", nullable = false)
    private Integer status;
    
    @Column(name = "ACTIVATION_DATE", nullable = true, columnDefinition= OFFSET_DATE_TIME_COLUMN_DEFINITION)
    @NotAudited
    private OffsetDateTime activationDate;    
    
    
    @Column(name = "SUBMITTED_ON_DATE", nullable = false, columnDefinition= OFFSET_DATE_TIME_COLUMN_DEFINITION, updatable = false)
    private OffsetDateTime submittedOnDate;
    
    @ManyToOne(optional = true, fetch=FetchType.LAZY)
    @JoinColumn(name = "SUBMITTED_BY_USER_ID", nullable = true)
    private AppUser submittedBy;    
    
    @Column(name = "REACTIVATION_DATE", nullable = true, columnDefinition= OFFSET_DATE_TIME_COLUMN_DEFINITION)
    private OffsetDateTime reactivationDate;
    
    @ManyToOne(optional = true, fetch=FetchType.LAZY)
    @JoinColumn(name = "REACTIVATED_BY_USER_ID", nullable = true)
    private AppUser reactivatedBy;
    
    @ManyToOne(optional = true, fetch=FetchType.LAZY)
    @JoinColumn(name = "ACTIVATED_BY_USER_ID", nullable = true)
    private AppUser activatedBy;

    @Column(name = "CLIENT_TYPE", nullable = false)
    @Enumerated(EnumType.STRING)
    private ClientType clientType;

    @OneToOne(optional = true, cascade = CascadeType.ALL, orphanRemoval = true)
    @JoinColumn(name = "CLIENT_INDIVIDUAL_ID", nullable = true)
    private ClientIndividual clientIndividual;
    
    @OneToOne(optional = false, cascade = CascadeType.ALL, orphanRemoval = true)
    @JoinColumn(name = "CLIENT_CONTACT_ID", nullable = false)
    private ClientContact clientContact;
    
    
	public Client() {
		super();
		// TODO Auto-generated constructor stub
	}



	public String getClientNumber() {
		return clientNumber;
	}



	public void setClientNumber(String clientNumber) {
		this.clientNumber = clientNumber;
	}



	public Integer getStatus() {
		return status;
	}



	public void setStatus(Integer status) {
		this.status = status;
	}



	public OffsetDateTime getActivationDate() {
		return activationDate;
	}



	public void setActivationDate(OffsetDateTime activationDate) {
		this.activationDate = activationDate;
	}

	public OffsetDateTime getReactivationDate() {
		return reactivationDate;
	}



	public void setReactivationDate(OffsetDateTime reactivationDate) {
		this.reactivationDate = reactivationDate;
	}



	public AppUser getReactivatedBy() {
		return reactivatedBy;
	}



	public void setReactivatedBy(AppUser reactivatedBy) {
		this.reactivatedBy = reactivatedBy;
	}



	public OffsetDateTime getSubmittedOnDate() {
		return submittedOnDate;
	}



	public void setSubmittedOnDate(OffsetDateTime submittedOnDate) {
		this.submittedOnDate = submittedOnDate;
	}



	public AppUser getSubmittedBy() {
		return submittedBy;
	}



	public void setSubmittedBy(AppUser submittedBy) {
		this.submittedBy = submittedBy;
	}


	public AppUser getActivatedBy() {
		return activatedBy;
	}



	public void setActivatedBy(AppUser activatedBy) {
		this.activatedBy = activatedBy;
	}



	public ClientType getClientType() {
		return clientType;
	}



	public void setClientType(ClientType clientType) {
		this.clientType = clientType;
	}



	public ClientIndividual getClientIndividual() {
		return clientIndividual;
	}



	public void setClientIndividual(ClientIndividual clientIndividual) {
		this.clientIndividual = clientIndividual;
	}



	public ClientContact getClientContact() {
		return clientContact;
	}



	public void setClientContact(ClientContact clientContact) {
		this.clientContact = clientContact;
	}

	
	
	
	
}
