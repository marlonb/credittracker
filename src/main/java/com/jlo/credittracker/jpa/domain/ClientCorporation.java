package com.jlo.credittracker.jpa.domain;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.validation.constraints.Size;

import org.hibernate.envers.Audited;

@Audited
@Entity()
public class ClientCorporation extends AbstractPersistableEntity<Long> implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1251445957199658920L;

    @Column(name = "COMPANY_NAME", length = 100)
    @Size(max = 100)
	protected String companyName;
    
    

	
	
}
