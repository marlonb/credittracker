package com.jlo.credittracker.jpa.domain.useradministration;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;

import org.hibernate.envers.Audited;

import com.jlo.credittracker.jpa.domain.AbstractPersistableEntity;

@Entity
@Audited
public class Permission extends AbstractPersistableEntity<Long> implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6725805094819108649L;

	@Column(name = "GROUPING", nullable = false, length = 45)
    private String grouping;

    @Column(name = "CODE", nullable = false, length = 100)
    private String code;

    @Column(name = "ENTITY_NAME", nullable = true, length = 100)
    private String entityName;

    @Column(name = "ACTION_NAME", nullable = true, length = 100)
    private String actionName;

    @Column(name = "CAN_MAKER_CHECKER_YN", nullable = false)
    private boolean canMakerCheckerYn;

	public Permission() {
		super();
		// TODO Auto-generated constructor stub
	}

	public String getGrouping() {
		return grouping;
	}

	public void setGrouping(String grouping) {
		this.grouping = grouping;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getEntityName() {
		return entityName;
	}

	public void setEntityName(String entityName) {
		this.entityName = entityName;
	}

	public String getActionName() {
		return actionName;
	}

	public void setActionName(String actionName) {
		this.actionName = actionName;
	}

	public boolean isCanMakerCheckerYn() {
		return canMakerCheckerYn;
	}

	public void setCanMakerCheckerYn(boolean canMakerCheckerYn) {
		this.canMakerCheckerYn = canMakerCheckerYn;
	}
    
    
}
