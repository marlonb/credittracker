package com.jlo.credittracker.jpa.domain;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.OneToOne;
import javax.validation.constraints.Size;

import org.hibernate.envers.Audited;

@Audited
@Entity()
public class ContactAddress extends AbstractPersistableEntity<Long> implements Serializable {

	
    /**
	 * 
	 */
	private static final long serialVersionUID = 4410745708914532889L;

	@Column(name = "BLOCK_NO", length = 10)
    @Size(max = 10)
    protected String blockNo;
    
    @Column(name = "BLOCK_NAME", length = 90)
    @Size(max = 90)
    protected String blockName;
    
    @Column(name = "STREET_NO", length = 10)
    @Size(max = 10)
    protected String streetNo;
    
    @Column(name = "STREET_NAME", length = 90)
    @Size(max = 90)
    protected String streetName;
    
    @Column(name = "ADDR1", length = 100)
    @Size(max = 100)
    protected String addr1;
    
    @Column(name = "ADDR2", length = 100)
    @Size(max = 100)
    protected String addr2;
    
    @Column(name = "ADDR3", length = 100)
    @Size(max = 100)
    protected String addr3;
    
    @Column(name = "ADDR4", length = 100)
    @Size(max = 100)
    protected String addr4;
    
    @Column(name = "ADDR5", length = 100)
    @Size(max = 100)
    protected String addr5;

    
    @Column(name = "DISTRICT", length = 100)
    @Size(max = 100)
    protected String district;
    
    @Column(name = "CITY", length = 100)
    @Size(max = 100)
    protected String city;
    
    @Column(name = "COUNTRY", length = 2)
    @Size(max = 2)
    protected String country;    
    
    @Column(name = "POSTAL_CODE", length = 20)
    @Size(max = 20)
    protected String postalCode;
    
    @Column(name = "PROVINCE", length = 100)
    @Size(max = 100)
    protected String province;
    
    @Column(name = "NEAREST_LANDMARK", length = 200)
    @Size(max = 200)
    protected String nearestLandmark;
    
    @OneToOne(mappedBy = "contactAddress")
    protected ClientContact clientContact;

	public String getBlockNo() {
		return blockNo;
	}

	public void setBlockNo(String blockNo) {
		this.blockNo = blockNo;
	}

	public String getBlockName() {
		return blockName;
	}

	public void setBlockName(String blockName) {
		this.blockName = blockName;
	}

	public String getStreetNo() {
		return streetNo;
	}

	public void setStreetNo(String streetNo) {
		this.streetNo = streetNo;
	}

	public String getStreetName() {
		return streetName;
	}

	public void setStreetName(String streetName) {
		this.streetName = streetName;
	}

	public String getAddr1() {
		return addr1;
	}

	public void setAddr1(String addr1) {
		this.addr1 = addr1;
	}

	public String getAddr2() {
		return addr2;
	}

	public void setAddr2(String addr2) {
		this.addr2 = addr2;
	}

	public String getAddr3() {
		return addr3;
	}

	public void setAddr3(String addr3) {
		this.addr3 = addr3;
	}

	public String getAddr4() {
		return addr4;
	}

	public void setAddr4(String addr4) {
		this.addr4 = addr4;
	}

	public String getAddr5() {
		return addr5;
	}

	public void setAddr5(String addr5) {
		this.addr5 = addr5;
	}

	public String getDistrict() {
		return district;
	}

	public void setDistrict(String district) {
		this.district = district;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getPostalCode() {
		return postalCode;
	}

	public void setPostalCode(String postalCode) {
		this.postalCode = postalCode;
	}

	public String getProvince() {
		return province;
	}

	public void setProvince(String province) {
		this.province = province;
	}

	public String getNearestLandmark() {
		return nearestLandmark;
	}

	public void setNearestLandmark(String nearestLandmark) {
		this.nearestLandmark = nearestLandmark;
	}

	public ClientContact getClientContact() {
		return clientContact;
	}

	public void setClientContact(ClientContact clientContact) {
		this.clientContact = clientContact;
	}
    
    
    
}
