package com.jlo.credittracker.jpa.domain;

import java.io.Serializable;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.validation.constraints.Size;

import org.hibernate.envers.Audited;

@Audited
@Entity()
public class ClientContact extends AbstractPersistableEntity<Long> implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -5860012103936843713L;
	
    @Column(name = "CONTACT_PERSON", length = 100)
    @Size(max = 100)
	protected String contactPerson;

    @Column(name = "CONTACT_DEPARTMENT", length = 100)
    @Size(max = 100)
    protected String contactDepartment;
    
    @OneToOne(optional = false, cascade = CascadeType.ALL, orphanRemoval = true)
    @JoinColumn(name = "CONTACT_ADDRESS_ID", nullable = false)
    protected ContactAddress contactAddress;
	
	@OneToOne(mappedBy = "clientContact")
    protected Client client;
	
	

	public String getContactPerson() {
		return contactPerson;
	}
	public void setContactPerson(String contactPerson) {
		this.contactPerson = contactPerson;
	}
	public String getContactDepartment() {
		return contactDepartment;
	}
	public void setContactDepartment(String contactDepartment) {
		this.contactDepartment = contactDepartment;
	}
	public ContactAddress getContactAddress() {
		return contactAddress;
	}
	public void setContactAddress(ContactAddress contactAddress) {
		this.contactAddress = contactAddress;
	}
	public Client getClient() {
		return client;
	}
	public void setClient(Client client) {
		this.client = client;
	}

}
