package com.jlo.credittracker;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import com.jlo.credittracker.jpa.repositories.ClientRepository;
import com.jlo.credittracker.jpa.repositories.useradministration.AppUserRepository;
import com.jlo.credittracker.jpa.repositories.useradministration.PermissionRepository;
import com.jlo.credittracker.jpa.repositories.useradministration.RoleRepository;


@SpringBootApplication
public class CreditTrackerApplication {

	public static void main(String[] args) {
		SpringApplication.run(CreditTrackerApplication.class, args);
	}
	
	@Bean
	public CommandLineRunner todoDemo(ClientRepository clientRepository, 
			PermissionRepository permissionRepository, 
			RoleRepository roleRepository,
			AppUserRepository appUserRepository) {
		return (args) -> {
			
			
		};
	}


}
